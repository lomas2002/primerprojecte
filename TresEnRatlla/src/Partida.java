import java.util.ArrayList;
import java.util.Scanner;

public class Partida {
    static int posi1 = 0;
    static int posi2 = 0;
    static int cont = 0;
    static Scanner lector = new Scanner(System.in);
    static String[][] tauler = new String[3][3];
    public static void comen�ar(){
        for (int f = 0; f < tauler.length; f++) {
            for (int c = 0; c < tauler[0].length; c++) {
        	tauler[f][c] = "|__";
            }
        }
    }
   
    public static void imprimir(){
        for (int f = 0; f < tauler.length; f++) {
            for (int c = 0; c < tauler[0].length; c++) {
                System.out.print(tauler[f][c]);
            }
            System.out.println("");
        }
    }

    public static void jugar(ArrayList<String> playerpartida, int posu, int posdos) {
	int fila = 0;
	int columna = 0;
	int fila1 = 0;
	int columna2 = 0;
	posi1 = posu;
	posi2 = posdos;
	cont = playerpartida.size();
	for (int i = 0; i < 4; i++) {
	    System.out.println("En quina fila vol " + playerpartida.get(posu) + " posar la creu? (de 0 a 2)");
	    fila1 = lector.nextInt();
	    System.out.println("En quina columna vol " + playerpartida.get(posu) + " posar la creu? (de 0 a 2)");
	    columna2 = lector.nextInt();

	    System.out.println("En quina fila vol " + playerpartida.get(posdos) + " posar la rodona? (de 0 a 2)");
	    fila = lector.nextInt();
	    System.out.println("En quina columna vol " + playerpartida.get(posdos) + " posar la rodona? (de 0 a 2)");
	    columna = lector.nextInt();
	    
	    if (tauler[fila][columna] == "|O_" || tauler[fila][columna] == "|X_" || tauler[fila1][columna2] == "|O_" || tauler[fila1][columna2] == "|X_") {
		System.out.println("Algun dels jugadors ha intentat posar una fitxa sobre de una altre");
		System.out.println("El que s'hagi equivocat");
		i--;
	    }else{
		tauler[fila1][columna2] = "|X_";
		tauler[fila][columna] = "|O_";
	    }
	    
	    if(i == 4) {
		System.out.println("En quina fila vol " + playerpartida.get(posu) + " posar la creu? (de 0 a 2)");
		fila1 = lector.nextInt();
		System.out.println("En quina columna vol " + playerpartida.get(posu) + " posar la creu? (de 0 a 2)");
		columna2 = lector.nextInt();
		if (tauler[fila1][columna2] == "|O_" || tauler[fila1][columna2] == "|X_") {
			System.out.println("Algun dels jugadors ha intentar posar una fitxa sobre de una altre");
			System.out.println("El que s'hagi equivocat");
			i--;
		    }else{
			tauler[fila1][columna2] = "|X_";
		    }
	    }
	    imprimir();
	    guanyar();
	}
    }
    public static int[] guanyar(){
	int guanyar1 = 0;
	int guanyar2 = 0;
	int [] partidesguanyades = new int[cont];
	
        if(tauler[0][0] == "|X_" && tauler[1][1] == "|X_" && tauler[2][2] == "|X_") {
            guanyar1++;
            partidesguanyades[posi1] = guanyar1;
        }else if (tauler[0][0] == "|O_" && tauler[1][1] == "|O_" && tauler[2][2] == "|O_") {
            guanyar2++;
            partidesguanyades[posi2] = guanyar2;
        }else if (tauler[0][0] == "|X_" && tauler[0][1] == "|X_" && tauler[0][2] == "|X_") {
            guanyar1++;
            partidesguanyades[posi1] = guanyar1;
        }else if (tauler[0][0] == "|O_" && tauler[0][1] == "|O_" && tauler[0][2] == "|O_") {
            guanyar2++;
            partidesguanyades[posi2] = guanyar2;
        }else if (tauler[1][0] == "|X_" && tauler[1][1] == "|X_" && tauler[1][2] == "|X_") {
            guanyar1++;
            partidesguanyades[posi1] = guanyar1;
        }else if (tauler[1][0] == "|O_" && tauler[1][1] == "|O_" && tauler[1][2] == "|O_") {
            guanyar2++;
            partidesguanyades[posi2] = guanyar2;
        }else if (tauler[2][0] == "|X_" && tauler[2][1] == "|X_" && tauler[2][2] == "|X_") {
            guanyar1++;
            partidesguanyades[posi1] = guanyar1;
        }else if (tauler[2][0] == "|O_" && tauler[2][1] == "|O_" && tauler[2][2] == "|O_") {
            guanyar2++;
            partidesguanyades[posi2] = guanyar2;
        }else if (tauler[0][0] == "|X_" && tauler[1][0] == "|X_" && tauler[2][0] == "|X_") {
            guanyar1++;
            partidesguanyades[posi1] = guanyar1;
        }else if (tauler[0][0] == "|O_" && tauler[1][0] == "|O_" && tauler[2][0] == "|O_") {
            guanyar2++;
            partidesguanyades[posi2] = guanyar2;
        }else if (tauler[0][1] == "|X_" && tauler[1][1] == "|X_" && tauler[2][1] == "|X_") {
            guanyar1++;
            partidesguanyades[posi1] = guanyar1;
        }else if (tauler[0][1] == "|O_" && tauler[1][1] == "|O_" && tauler[2][1] == "|O_") {
            guanyar2++;
            partidesguanyades[posi2] = guanyar2;
        }else if (tauler[0][2] == "|X_" && tauler[1][2] == "|X_" && tauler[2][2] == "|X_") {
            guanyar1++;
            partidesguanyades[posi1] = guanyar1;
        }else if (tauler[0][2] == "|O_" && tauler[1][2] == "|O_" && tauler[2][2] == "|O_") {
            guanyar2++;
            partidesguanyades[posi2] = guanyar2;
        }else {
            lector.nextLine();
        }
	return partidesguanyades;
    }
}
